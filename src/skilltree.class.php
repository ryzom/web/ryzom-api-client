<?php

/**
 * Class Skilltree
 *
 * Parse skills from flat xml to array tree
 */
class Skilltree
{
    public static $classFinished = ' finished';
    public static $classUnfinished = ' unfinished';

    public static $tplOuter = '<ul>{skills}</ul>';
    public static $tplInner = '<li><div class="node{class}">{name}<span class="level">{level}</span>{progress}</div>{childs}</li>';
    public static $tplProgress = '<br><div class="progress"><div class="progress-bar" style="width:{percent-done}%;"></div></div>';

    /**
     * @param SimpleXMLElement $xml
     *
     * @return array
     */
    public static function parse(SimpleXMLElement $xml)
    {
        $tree = array();
        foreach ($xml->children() as $node) {
            /** @var $node SimpleXMLElement */
            $name = (string) $node->getName();

            $skill = array(
                'level' => (int) $node,
                'childs' => array()
            );

            if ($skill['level'] <= 20) {
                $skill['min_level'] = 0;
                $skill['max_level'] = 20;
            } elseif ($skill['level'] <= 50) {
                $skill['min_level'] = 20;
                $skill['max_level'] = 50;
            } elseif ($skill['level'] <= 100) {
                $skill['min_level'] = 50;
                $skill['max_level'] = 100;
            } elseif ($skill['level'] <= 150) {
                $skill['min_level'] = 100;
                $skill['max_level'] = 150;
            } elseif ($skill['level'] <= 200) {
                $skill['min_level'] = 150;
                $skill['max_level'] = 200;
            } else {
                $skill['min_level'] = 200;
                $skill['max_level'] = 250;
            }

            $tree[$name] = $skill;
        }
        ksort($tree);

        foreach ($tree as $name => $level) {
            for ($i = strlen($name) - 1; $i > 0; $i--) {
                $sub = substr($name, 0, $i);
                if (isset($tree[$sub])) {
                    $tree[$sub]['childs'][$name] =& $tree[$name];
                    break;
                }
            }
        }
        // get the root nodes and 'sort' them in the right order,
        // sub-skills should be ordered correctly
        return array(
            'sf' => $tree['sf'],
            'sm' => $tree['sm'],
            'sc' => $tree['sc'],
            'sh' => $tree['sh'],
        );
    }

    /**
     * @param SimpleXMLElement $xml
     * @param string           $lang
     *
     * @return string
     */
    public static function render(SimpleXMLElement $xml, $lang = 'en')
    {
        $skilltree = self::parse($xml);

        return self::renderTree($skilltree, $lang);
    }

    /**
     * @param array  $tree
     * @param string $lang
     *
     * @return string
     */
    public static function renderTree(array $tree, $lang = 'en')
    {
        $skills = '';
        foreach ($tree as $key => $node) {
            $name = ryzom_translate($key . '.skill', $lang);

            // check if skill is finished, or is not
            if (empty($node['childs']) && $node['level'] != 250) {
                $class = self::$classFinished;
                $level = $node['level'] . '/' . $node['max_level'];

                $percent = (($node['level'] - $node['min_level']) / ($node['max_level'] - $node['min_level'])) * 100;
                $pb = strtr(
                    self::$tplProgress,
                    array(
                        '{percent-done}' => sprintf('%.3f', $percent),
                        '{percent-left}' => sprintf('%.3f', 100 - $percent),
                    )
                );
            } else {
                $class = self::$classUnfinished;
                $level = $node['level'];
                $pb = '';
            }

            $childs = '';
            if (!empty($node['childs'])) {
                $childs .= self::renderTree($node['childs'], $lang);
            }

            $skills .= strtr(
                self::$tplInner,
                array(
                    '{class}' => $class,
                    '{name}' => $name,
                    '{level}' => $level,
                    '{progress}' => $pb,
                    '{childs}' => $childs,
                )
            );
        }

        return strtr(self::$tplOuter, array('{skills}' => $skills));
    }
}
