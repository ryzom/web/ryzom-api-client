<?php
/**
 * RyzomAPI example
 */

require_once dirname(__FILE__) . '/../lib/ryzomapi_lite.php';
require_once dirname(__FILE__) . '/skilltree.class.php';

if (!function_exists('ryzom_translate')) {
    /**
     * ryzom_translation function is from ryzom_extra library
     * https://github.com/nimetu/ryzom_extra
     *
     * create placeholder function
     */
    function ryzom_translate($sheet, $lang)
    {
        return $sheet;
    }
}

$ig = isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'], 'Ryzom') !== false;
if (isset($_REQUEST['lang'])) {
    // ingame language
    $lang = $_REQUEST['lang'];
} else if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
    // outgame language
    $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
} else {
    $lang = '';
}
if (!in_array($lang, array('en', 'fr', 'de', 'es', 'ru'))) {
    // fallback
    $lang = 'en';
}
$info = '';
$error = '';
$apikey = '';
$inventory = '';
if (isset($_POST['apikey']) && !empty($_POST['apikey'])) {
    $apikey = $_POST['apikey'];

    $xml = false;
    $result = ryzom_character_api($apikey);
    if (isset($result[$apikey])) {
        $xml = $result[$apikey];
    }

    if ($xml === false || isset($xml->error)) {
        $error = 'ERROR: apikey error<br>';
        if (isset($xml->error)) {
            $error .= sprintf('Server said: [%d]: %s<br>', (int) $xml->error['code'], h((string) $xml->error));
        }
    } else {
        $apikey = (string) $xml['apikey'];
        if (strstr($xml['modules'], 'C01') || strstr($xml['modules'], 'C02')) {
            $info .= '<h2>Character</h2>';
            $info .= render_character_info($xml);
        }
        if (strstr($xml['modules'], 'C04')) {
            $info .= '<h2>Bag</h2>';
            // ingame browser bug: <img> fails to render without some whitespace
            $info .= ' ' . render_inventory($xml->bag);
        }
        if (strstr($xml['modules'], 'C05')) {
            $info .= '<h2>Room</h2>';
            // ingame browser bug: <img> fails to render without some whitespace
            $info .= ' ' . render_inventory($xml->room);
        }
        if (strstr($xml['modules'], 'A03')) {
            $info .= '<h2>Packers</h2>';
            foreach ($xml->pets->animal as $pet) {
                if ((string)$pet->status === 'not_present') {
                    continue;
                }
                $info .= '<h3>Packer #' . intval($pet['index']) . '</h3>';
                $info .= '<table width="100%"><tr valign="top">';
                $info .= '<td width="80%">' . render_inventory($pet->inventory) . '</td>';
                if (isset($pet->position)) {
                    if ((string)$pet->status === 'stable') {
                        $icon = 'building';
                    } else {
                        $icon = 'mektoub';
                    }
                    $info .= '<td width="20%">' . render_map(
                            'Packer ' . (int) $pet['index'],
                            $icon,
                            $pet->position
                        ) . '</td>';
                }
                $info .= '</table>';
            }
        }
        if (strstr($xml['modules'], 'P02')) {
            if ($ig) {
                Skilltree::$classFinished = '#747f1f';
                Skilltree::$classUnfinished = '#8a5617';

                Skilltree::$tplOuter = '
                <table>
                    <tr>
                        <td height="1" width="30">&nbsp;</td>
                        <td>{skills}</td>
                    </tr>
                </table>';
                Skilltree::$tplInner = '
                <table bgcolor="{class}" width="200" cellspacing="0" cellpadding="0">
                    <tr valign="middle">
                        <td width="10" height="30"></td>
                        <td>
                            <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="80%">{name}</td>
                                    <td width="20%">{level}</td>
                                </tr>
                            </table>
                            {progress}
                        </td>
                        <td width="10"></td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td height="1"></td>
                    </tr>
                </table>
                {childs}';
                Skilltree::$tplProgress = '
                <table bgcolor="#222222" width="180" cellspacing="0" cellpadding="1">
                    <tr>
                        <td bgcolor="#888888" width="{percent-done}%" height="5"></td>
                        <td width="{percent-left}%"></td>
                    </tr>
                </table>';
            } else {
                $info .= '
                <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                <script type="text/javascript">
                    // <![CDATA[
                    $(function () {
                        $("#skilltree")
                                .find(".node").click(function () {
                                    $(this).parent().find(">ul").toggle();
                                }).end()
                                .find("li>ul").hide();
                    });
                    // ]]>
                </script>';
            }
            $info .= '<h2>Skilltree</h2>';
            $info .= '<div id="skilltree">' . Skilltree::render($xml->skills, $lang) . '</div>';
        }
    }
}

if (!empty($error)) {
    $error = '<p style="color: red;">' . $error . '</p>';
}

$tpl = '
<form method="post" action="{url}">
	<label>
		<h1>Character API key</h1>
		{error}
		<input type="text" name="apikey" value="{apikey}" size="{size}">
	</label>
	<br>
	<input type="submit" name="submit" value="submit">
</form>
<hr>
{info}
';
$content = strtr(
    $tpl,
    array(
        '{error}' => $error,
        '{apikey}' => h($apikey),
        '{info}' => $info,
        '{size}' => $ig ? 600 : 60,
        '{url}' => $_SERVER['PHP_SELF'],
    )
);

$tpl = '
<!doctype html>
<html>
	<head>
		<title>Ryzom API | Example</title>
		<link type="text/css" href="https://api.ryzom.com/data/css/ryzom_ui.css" rel="stylesheet" media="all" />
	</head>
	<style type="text/css">
	html, body {
		max-width: 800px;
		margin: 0 auto;
	}
	body {
		background-color: #000010;
	}

	#skilltree .node {
		position: relative;
		width: 200px;
		height: 30px;
		border: 1px solid black;
		display: block;
		margin: 1px;
		font-size: 10px;
		text-align: center;
		color: white;
		cursor: pointer;
	}

	#skilltree .node:hover {
		background-color: #abcdef;
	}

	#skilltree .level {
		position: absolute;
		right: 2px;
		padding-right: .3em;
	}

	#skilltree .progress {
		display: inline-block;
		box-sizing: border-box;
		border: 1px #333;
		padding: 1px;
		background-color: #000;
		width: 90%;
		margin-top: 2px;
	}
	#skilltree .progress-bar {
		background-color: #888;
		height: 5px;
	}

	#skilltree .finished {
		background-color: #8a5617;
	}

	#skilltree .unfinished {
		background-color: #747f1f;
	}
	</style>
	<body>
	{content}
	</body>
</html>
';

header('Content-Type: text/html; charset=utf-8');
echo strtr(
    $tpl,
    array(
        '{content}' => ryzom_render_window('Ryzom API | Example', $content),
    )
);

/**
 * General info about character
 *
 * @param SimpleXMLElement $xml
 *
 * @return string
 */
function render_character_info(SimpleXMLElement $xml)
{
    $rows = array();
    $rows[] = 'Name: ' . h((string) $xml->name);
    if (isset($xml->race)) {
        // C01
        $rows[] = 'Race: ' . (string) $xml->race;
        $rows[] = 'Played: ' . string_from_seconds((int) $xml->played);
        if (isset($xml->guild)) {
            $rows[] = 'Guild: ' . h((string) $xml->guild->name) . ' (' . (string) $xml->guild->grade . ')';
            $rows[] = '<img src="' . ryzom_guild_icon_url((string) $xml->guild->icon, 'b') . '" alt="">';
        }
        $dr = build_dressingroom_url($xml);
    } else {
        $dr = '';
    }

    if (isset($xml->position)) {
        // C02
        $rows[] = render_map((string) $xml->name, 'npc', $xml->position);
    }

    $tpl = '
	<table width="100%">
	<tr valign="top">
		<td>{rows}</td>
		<td><img src="{image}" alt="" width="200"></td>
	</tr>
	</table>';

    return strtr(
        $tpl,
        array(
            '{rows}' => join('<br>', $rows),
            '{image}' => $dr,
        )
    );
}

/**
 * Render items from inventory
 *
 * @param SimpleXMLElement $xml
 *
 * @return string
 */
function render_inventory(SimpleXMLElement $xml)
{
    $result = '';
    foreach ($xml->item as $item) {
        $url = ryzom_item_icon_url(
            (string) $item->sheet,
            (int) $item->craftparameters->color,
            (int) $item->quality,
            (int) $item->stack,
            -1,
            intval($item->hp) == 1,
            true,
            intval($item->locked) == 1
        );
        $result .= '<img src="' . $url . '" style="border: 1px solid #333;" alt="">';
    }

    return $result;
}

/**
 * Return location map image
 *
 * @param string           $name
 * @param string           $icon
 * @param SimpleXMLElement $pos
 *
 * @return string
 */
function render_map($name, $icon, SimpleXMLElement $pos)
{
    $params = array(
        'markers' => (int) $pos['x'] . ',' . (int) $pos['y'] . '|label:' . $name . '|icon:' . $icon,
        'size' => '300x150',
        'language' => 'auto',
    );

    return '<img src="https://api.bmsite.net/maps/static?' . http_build_query($params) . '" alt="">';
}

/**
 * Build character dressing room image url
 *
 * @param SimpleXMLElement $xml
 *
 * @return string
 */
function build_dressingroom_url(SimpleXMLElement $xml)
{
    // body size
    $gabarit = array();
    foreach (array('height', 'torso', 'arms', 'legs', 'breast') as $field) {
        $gabarit[] = (int) $xml->body->gabarit[$field];
    }

    $morph = array();
    for ($i = 1; $i < 9; ++$i) {
        $morph[] = (int) $xml->body->morph['target' . $i];
    }

    // appearance
    $params = array(
        'dir' => '0',
        'race' => substr((string) $xml->race, 0, 2),
        'gender' => (string) $xml->gender,
        'hair' => (int) $xml->body->hairtype . '/' . (int) $xml->body->haircolor,
        'tattoo' => (int) $xml->body->tattoo,
        'eyes' => (int) $xml->body->eyescolor,
        'gabarit' => join(',', $gabarit),
        'morph' => join(',', $morph),
    );

    // armor, hands
    foreach (array('head', 'chest', 'arms', 'hands', 'feet', 'legs', 'handr', 'handl') as $field) {
        if (isset($xml->equipment->{$field})) {
            $node = $xml->equipment->{$field};
            $params[$field] = (string) $node;
            if (isset($node['color'])) {
                $params[$field] .= '/' . $node['color'];
            }
        }
    }

    return 'https://api.bmsite.net/char/render?' . http_build_query($params);
}

/**
 * Return human readable string from seconds
 *
 * @param int $seconds
 *
 * @return string
 */
function string_from_seconds($seconds)
{
    $units = array(
        'd' => 3600 * 24,
        'h' => 3600,
        'm' => 60,
        's' => 1,
    );
    if ($seconds == 0) {
        return '0s';
    }
    $result = '';
    foreach ($units as $txt => $div) {
        $nr = floor($seconds / $div);
        if ($nr > 0) {
            $result .= $nr . $txt;
            $seconds -= $nr * $div;
        }
    }

    return $result;
}

/**
 * Escape html special chars making string safe to html
 *
 * @param string $str
 *
 * @return string
 */
function h($str)
{
    return htmlspecialchars($str, ENT_QUOTES, 'UTF-8');
}
